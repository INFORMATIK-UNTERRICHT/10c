class Regenwurm{
    // Ein Kommentar wird eingeleitet durch //
    // Attribute
    private int lebensenergie = 15;
    // int steht für integer 
    // und das sind alle ganzen Zahlen
    private int anzahlGelegterEier = 0;
    // String steht für Texte, d.h.
    // mehrere Buchstaben (Datentyp des Buchstabens char)
    private String aktuellesGeschlecht = "weiblich";
    // Attribute Ende
    // -----------------------------
    // Konstruktoren
    // Konstruktoren sind Methoden, die einmalig 
    // nur bei der Erzeugung eines Objektes ausgeführt
    // werden können.
    // Gibt es mehrere Konstruktoren, kann nur einer
    // ausgeführt werden.
    public Regenwurm(){
        this.aktuellesGeschlecht = "weiblich oder so";
        this.anzahlGelegterEier = -2000;
        this.lebensenergie = 250;
    }
    // Gibt es mehrere Konstruktoren unterscheiden sich diese
    // nur an Hand ihrer Paremterliste. Es kann dann aber
    // nur einer aufgerufen werden.
    // In den Konstruktoren können Wertzuweisungen oder 
    // Botschaften ausgeführt werden.
    public Regenwurm(int lebensEnergiePar){
        // Wertzuweisung: Weist dem Attribut lebensenergie
        // den Inhalt des Parameters lebensEnergiePar zu.
        this.lebensenergie = lebensEnergiePar;
        // Ruft die Methode teste mit "männlich" und 100
        // für die Parameter geschlechtParameter und
        // anzahlGelegterEierParameter auf.
        this.teste("männlich", 100);
    }
    
    // Konstruktoren Ende
    // -----------------------------
    // Methoden
    public void legeEinEi(){
        anzahlGelegterEier = 1;
    }

    public void legeHundertEier(){
        // Dies hier ist eine sogenannte Wertzuweisung
        // Dem Attribut anzahlGelegterEier wird der Wert
        // Rechts vom IstgleichZeichen zugeordnet.
        // += nimmt den bereits vorhandenen Wert und erhöht ihn
        // um die Zahl, die Rechts von der Wertzuweidung steht.
        anzahlGelegterEier += 100;
        // Alternative Schreibweise
        // anzahlGelegterEier = anzahlGelegterEier + 100;
    }

    // In der folgenden Methode soll der Benutzer erst die
    // Anzahl der zu legenden Eier angeben.
    // Dieser Wert wird in dem Parameter anzahlEierParameter
    // "zwischengespeichert" und in der folgenden Wertzu-
    // weisung in das Attribut anzahlGelegterEier übertragen.
    // Am Ende der Methode wird der Parameter
    // AnzahlEierParameter wieder gelöscht. 
    public void legeAnzahlEier(int anzahlEierParameter){
        anzahlGelegterEier += anzahlEierParameter;
    } 

    public void seiWeiblich(){
        aktuellesGeschlecht = "weiblich";
    }

    public void seiMännlich(){
        aktuellesGeschlecht = "männlich";
    }
    
    // Setze Methoden werden im Englischen auch
    // set Methoden genannt
    public void setzeGeschlechtAuf(String geschlechtParameter){
        aktuellesGeschlecht = geschlechtParameter;
    }
    
    public void teste(String geschlechtParameter, int anzahlGelegterEierParameter){
        this.legeAnzahlEier(anzahlGelegterEierParameter);
        this.setzeGeschlechtAuf(geschlechtParameter);
    }
    
    public void informationenAusgeben(){
        System.out.println("----------------------------------------------");
        System.out.println("Dies ist mein Zustand: ");
        System.out.println("Attribut lebensenergie: " + lebensenergie);
        System.out.println("Attribut aktuellesGeschlecht: " + aktuellesGeschlecht);
        System.out.println("Attribut anzahlGelegterEier: " + anzahlGelegterEier);
        System.out.println("Meine Klasse ist: " + this.getClass().toString());
    }
    
    // get Methoden kann man im Deutschen auch als
    // erhalte oder bekomme Methoden bezeichnen.
    public int getLebensenergie(){
        // Bei Methoden mit einem Rückgabewert
        // erhält der Aufrufende das,was nach return steht.
        // Statt void schreibt man nun den Datentyp des
        // Rückgabewertes.
        return this.lebensenergie;
    }
    
    public void setLebensenergie(int lebensenergiePar){
        this.lebensenergie = lebensenergiePar;
    }
    // Methoden Ende
}










