class Terrarium{
    // Attribute
    // Statt Standarddatentypen wie int oder
    // String benutzen wir jetzt als
    // Datentypen die von uns programmierten Klassen
    // Regenwurm und Riesenschnecke
    // Solche Attribute nennt man dann 
    // REFERENZATTRIBUTE
    private int anzahlBewohner;
    private Regenwurm wurmi;
    private Regenwurm horsti;
    private Regenwurm franzi;
    private Riesenschnecke schneckerl;
    private Riesenschnecke rambo;
    private Froesche hans;
    private Froesche lutz;
    // Array vom Datentyp Ameise
    private Ameise[] ameisenVolk;
    private int anzahlAmeisen = 0;
    // Attribute Ende
    // ----------------------------------------
    // Konstruktoren
    // Standardkonstruktor = leere Parameterliste
    public Terrarium(){
        // Dem Referenzatribut wurmi wird jetzt ein
        // konkretes Objekt der Klasse Regenwurm zu-
        // geordnet. Dieses müssen wir jedoch zuerst
        // durch new und dem Aufruf eines Konstruktors
        // erzeugen.
        wurmi = new Regenwurm();
        horsti = new Regenwurm(220000);
        franzi = new Regenwurm();
        rambo = new Riesenschnecke();
        schneckerl = new Riesenschnecke();
        // hans als Objekt der Klasse Froesche wird
        // erzeugt indem man den Standardkonstruktor 
        // der Klasse Froesche aufruft
        hans = new Froesche();
        // lutz wird erzeugt durch Aufruf des Nichts-
        // Standardkonstrukotrs
        lutz = new Froesche(12,"schmierig-braun",false);
        ameisenVolk = new Ameise[10];
        ameisenVolk[0] = new Ameise(4,"Arbeiterin");
        this.anzahlAmeisen++;
        ameisenVolk[1] = new Ameise(6,"Königin");     
        this.anzahlAmeisen++;
    }

    public Terrarium(Ameise neueAmeisePlatz0Par, Ameise neueAmeisePlatz1Par ){
        // Dem Referenzatribut wurmi wird jetzt ein
        // konkretes Objekt der Klasse Regenwurm zu-
        // geordnet. Dieses müssen wir jedoch zuerst
        // durch new und dem Aufruf eines Konstruktors
        // erzeugen.
        wurmi = new Regenwurm();
        horsti = new Regenwurm(220000);
        franzi = new Regenwurm();
        rambo = new Riesenschnecke();
        schneckerl = new Riesenschnecke();
        // hans als Objekt der Klasse Froesche wird
        // erzeugt indem man den Standardkonstruktor 
        // der Klasse Froesche aufruft
        hans = new Froesche();
        // lutz wird erzeugt durch Aufruf des Nichts-
        // Standardkonstrukotrs
        lutz = new Froesche(12,"schmierig-braun",false);
        ameisenVolk = new Ameise[10];
        ameisenVolk[0] = neueAmeisePlatz0Par;
        this.anzahlAmeisen++;
        ameisenVolk[1] = neueAmeisePlatz1Par;     
        this.anzahlAmeisen++;
    }

    public Terrarium(
    int lebensenergieWurmiPar,
    int lebensenergieFranziPar,
    int lebensenergieHorstiPar,
    int lebensenergieLutzPar,
    int lebenesnergieCassandraPar){
        wurmi = new Regenwurm(lebensenergieWurmiPar);
        horsti = new Regenwurm(lebensenergieHorstiPar);
        franzi = new Regenwurm(lebensenergieFranziPar);
        rambo = new Riesenschnecke();
        schneckerl = new Riesenschnecke();
        hans = new Froesche();
        lutz = new Froesche(
            lebensenergieLutzPar,
            "schmierig-braun",
            false);
        ameisenVolk = new Ameise[10];
        ameisenVolk[0] = new Ameise(4,"Arbeiterin");
        this.anzahlAmeisen++;
        ameisenVolk[1] = new Ameise(lebenesnergieCassandraPar,"Königin");    
        this.anzahlAmeisen++;
    }
    // Konstruktoren Ende
    // ----------------------------------------
    // Methoden
    public void setLebensenergieWurmi (int lebensenergiePar){
        this.wurmi.setLebensenergie(lebensenergiePar);
    }

    public void infosAusgeben(){
        // Da horsti ein Objekt der Klasse Froesche ist, wird folglich hier auch
        // die Methode informationenAusgeben() der Klasse Froesche aufgerufen.
        this.horsti.informationenAusgeben();
        // Da franzi ein Objekt der Klasse Froesche ist, wird folglich hier auch
        // die Methode informationenAusgeben() der Klasse Froesche aufgerufen.
        this.franzi.informationenAusgeben();
        // Da wurmi ein Objekt der Klasse Froesche ist, wird folglich hier auch
        // die Methode informationenAusgeben() der Klasse Regenwurm aufgerufen.
        this.wurmi.informationenAusgeben();
        // Da rambo ein Objekt der Klasse Froesche ist, wird folglich hier auch
        // die Methode informationenAusgeben() der Klasse Regenwurm aufgerufen.
        this.rambo.informationenAusgeben();
        // Da schneckerl ein Objekt der Klasse Froesche ist, wird folglich hier auch
        // die Methode informationenAusgeben() der Klasse Riesenschnecke aufgerufen.
        this.schneckerl.informationenAusgeben();
        // Infos ausgeben im Array
        this.ameisenVolk[0].informationenAusgeben();
        this.ameisenVolk[1].informationenAusgeben();
    }

    public void neueAmeiseHinzufuegen(Ameise ameiseNeuPar){
        // Zweiseitige Auswahl. Wenn die Bedingung erfüllt
        // ist, wird der erste Abschnitt ausgeführt, ansonsten
        // der zweite (else) Teil.       
        if(this.anzahlAmeisen < 10){
            // Die neue Ameise soll an die erste leere Stelle
            // gesetzt werden. Da mit der Zählung bei Null 
            // begonnen wird ist die Anzahl der belegten Fächer
            // zugleich die Nummer des nächsten freien Faches.
            this.ameisenVolk[this.anzahlAmeisen] = ameiseNeuPar;
            // DANACH kann das Attribut anzahlAmeisen um Eins
            // erhöht werden
            this.anzahlAmeisen++;
        }
        // Der erste Teil wird übersprungen, wenn die Bedingung
        // NICHT erfüllt ist. Also, wenn bereits 10 (oder mehr Ameisen
        // Objekte im Array sind un d der Wert der integer Variable
        // anzahlAmeisen größer oder gleich 10 ist.
        else{
            // Der folgende Text wird auf der Konsole in roter
            // Schrift als sogenannte Fehlermeldung ausgegeben.
            // Statt out --> err.
            System.err.println
            ("Kein Platz mehr für eine weitere Ameise");
        }
    }

    public void alleInformationenAusgeben(){
        if(this.wurmi != null){
            this.wurmi.informationenAusgeben();
        }
        if(this.horsti != null){
            this.horsti.informationenAusgeben();
        }
        if(this.franzi != null){
            this.franzi.informationenAusgeben();
        }
        if(this.schneckerl != null){
            this.schneckerl.informationenAusgeben();
        }
        if(this.rambo != null){
            this.rambo.informationenAusgeben();
        }
        if(this.hans != null){
            this.hans.informationenAusgeben();
        }
        if(this.lutz != null){
            this.lutz.informationenAusgeben();
        }
        int i = 0;
        while(i < 10){
            if(this.ameisenVolk[i] != null){
                this.ameisenVolk[i].informationenAusgeben();
            }
            i++;
        }
    }
    // Methoden Ende
}


