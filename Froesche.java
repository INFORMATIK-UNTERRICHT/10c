class Froesche {
  // Dist ein Kommentar im Java Editor
    // Attribute
    // Hier werden die Attribute vorgestellt
    // -> DEKLARATION
    private int lebensenergie;
    private String hautfarbe;
    private boolean giftig; //Entweder true oder false 
    // Attribute Ende
    // ---------------------
    // Konstruktoren
    // Im Konstruktor werden die Attribute mit
    // Werten versehen -> INITIALISIERUNG
    public Froesche(){
        // Ohne Wertzuweisung haben die Attribute
        // automatisch den Standardwert
        this.lebensenergie = 23;
        this.hautfarbe = "schwarz-gelb";
        this.giftig = true;
    }

    public Froesche ( 
    int lebensenergiePar,
    String hautfarbePar,
    boolean giftigPar){
        this.lebensenergie = lebensenergiePar;
        this.hautfarbe = hautfarbePar;
        this.giftig = giftigPar; 
    }
    // Konstruktoren Ende
    // ---------------------
    // Methoden
    public void quacken(){
        // ++ erhoeht einen integer Wert um 1
        this.lebensenergie++;
    }

    public void informationenAusgeben(){
        System.out.println("----------------------------------------------");
        System.out.println("Ich bin von der Klasse " +
            this.getClass().toString());
        System.out.println("Meine Lebensenergie ist: " +
            this.lebensenergie);
        System.out.println("Ich bin gitfig: " + this.giftig);
    }
    // Methoden Ende
    // ---------------------    
}
