import javax.swing.ImageIcon;
import javax.swing.JOptionPane; 

class Ameise{
    // Attribute: lebensenergie, rolle (Arbeiter, Soldat,
    // Männchen oder Königin) und geschlecht
    // Konstruktoren: Standardkonstruktor, 
    // Konstruktor zur Festsetzung lebensenergie und rolle
    // Methoden: set und getMethode lebensenergie,
    // informationenAusgeben

    // Attribute nennt man auch GLOBALE VARIABLEN
    // Global deswegen, weil in ALLEN Methoden darauf zurückgegriffen werden kann
    private int lebensenergie;
    private String geschlecht;
    private String rolle;
    // Attribute Ende
    // -------------------------------------------------
    // Konstruktoren
    // Standardkonstruktor (= leere Parameterliste)
    public Ameise(){
        this.lebensenergie= 10;
        this.geschlecht = "weiblich";
        this.rolle = "Arbeiterin";
    }

    // Nichtstandardkonstruktor, weil die Parameter lebensenergie und rolle vorhanden sind
    // Parameter sind LOKALE VARIABLEN
    // Parameter sind nur bis zume Ende des Konstruktors 
    // oder der jeweiligen Methode gültig
    public Ameise(int lebensenergie, String rolle){
        // Mit this.lebensenergie ist das Attribut gemeint,
        // mit lebensengergie hingegen der Parameter.
        // D.h. hier wird der Inhalt des Parameters lebensenergie in
        // das Attribut lebensenergie übertragen.
        this.lebensenergie = lebensenergie;
        this.geschlecht = "weiblich";
        this.rolle = rolle;
    }
    // Konstruktoren Ende
    public void setLebensenergie(int lebensenergie){
        // Verändere die Methode so, dass der Benutzer keinen Wert
        // setzen darf, der größer oder gleich 20 ist.
        // Falls dies geschieht, soll auf der Konsole eine Fehlermeldung
        // ausgegeben werden.
        if(lebensenergie < 20){
            this.lebensenergie = lebensenergie;
        }
        else{
            // Damit Messagefeld erscheint VOR Klasse folgende
            // Importe durchführen
            // import javax.swing.ImageIcon;
            // import javax.swing.JOptionPane; 
            JOptionPane.showMessageDialog(null,"Wert war zu groß!"); 
        }
    }

    public int getLebensenergie(){
        return this.lebensenergie;
    }

    public void informationenAusgeben(){
        System.out.println("Dies ist mein Zustand: ");
        System.out.println("Attribut lebensenergie: " + this.lebensenergie);
        System.out.println("Attribut geschlecht: " + this.geschlecht);
        System.out.println("Attribut rolle: " + this.rolle);
        System.out.println("Meine Klasse ist: " + this.getClass().toString());
    }
}

