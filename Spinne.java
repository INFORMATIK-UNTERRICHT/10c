class Spinne extends Tier{
    // Attribute

    // Attribute Ende
    // Konstruktoren
    public Spinne(){
        // Erstmal benötigen wir das innere Objekt.
        // Da wir von Tier erben, wird erstmal ein Objekt
        // der Klasse Tier erzeugt (mit Hilfe super).
        super();
        this.anzahlBeine = 8;
    }

    public Spinne(int lebensEnergiePar){
        // Jetzt erzeugen wir das innere Objekt, indem wir den Nichtstandard
        // Konstruktor der Klasse Tier aufrufen (der einen Wert für den Parameter
        // lebensEnergiePar erwartet)
        super(lebensEnergiePar);
        this.anzahlBeine = 8;
    }
    // Konstruktoren Enden
    // Methoden
    public void werBinIch(){
        System.out.println("Ich bin eine Spinne!");
    }
    // Methoden Ende
}