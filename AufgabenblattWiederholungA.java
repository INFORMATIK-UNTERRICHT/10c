class AufgabenblattWiederholungA{
    // Attribute

    // Attribute Ende
    // ----------------------
    // Konstruktoren
    public AufgabenblattWiederholungA(){
        // Man gibt einen Konstruktor an, auch wenn man keinen braucht.
        // GUTER PROGRAMMIERSTIL
    }
    // Konstruktoren Ende
    // ----------------------
    // Methoden
    //Aufgabe berechneLaufzeit
    public int aufgabe1(double anfangsKapital, double endKapital, double zinssatz){
        int anzahlJahreLok = 0;
        double aktuellesKapitalLok = anfangsKapital;
        while(aktuellesKapitalLok <= endKapital){
            aktuellesKapitalLok = aktuellesKapitalLok * ( 1 + zinssatz);
            anzahlJahreLok++;
        }
        return anzahlJahreLok;
    }

    public int aufgabe2(int a, int b){
        while(a >= b){
            a -= b;
        }
        return a;
        // Bedeutung: Berechnung des Restes bei Division
    }

    public int aufgabe3(int a, int b){
        int anzahlSubtraktionenLok = 0;
        while(a >= b){
            a -= b;
            anzahlSubtraktionenLok++;
        }
        return anzahlSubtraktionenLok;
        // Bedeutung: Berechnung des Restes bei Division
    }

    public void aufgabe4(int a, int b){
        while(a != b){
            if(a > b){
                a = a - b;
            }
            else{
                b = b - a;
            }
        }
    }

    public double aufgabe5(double anfangsKapital, double jahresrate, double zinssatz){
        double aktuellesKapitalLok = anfangsKapital;
        while(aktuellesKapitalLok >= jahresrate){
            aktuellesKapitalLok = aktuellesKapitalLok * ( 1 + zinssatz) - jahresrate;
        }
        // Rückgabe des Restwertes des Kapitals, welches kleiner als die jahresrate ist
        return aktuellesKapitalLok;
    }
    // Methoden Ende
    // ----------------------
}