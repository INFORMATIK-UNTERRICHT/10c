class Wiederholung{
    // Attribute

    // Attribute Ende
    // ----------------------
    // Konstruktoren
    public Wiederholung(){
        // Konstruktor macht hier nix
        // Gehört sich aber so
    }
    // Kosntruktoren Ende
    // ----------------------
    // Methode
    public void wiederholungA(int anzahlWiederholungenPar){
        // Anlegen einer lokalen Variable zaehlerLok
        int zaehlerLok = 1;
        while( zaehlerLok <= anzahlWiederholungenPar){
            System.out.println("Das ist die " + zaehlerLok + "te Wiederholung.");
            zaehlerLok++;
        }
    }

    public void wiederholungE(int anzahlWiederholungenPar){
        // Anlegen einer lokalen Variable zaehlerLok
        int zaehlerLok = 1;
        // Bei der Wiederholung mit Endbedingung wird die Anweisung MINDESTENS einmal ausge-
        // führt.
        do{
            System.err.println("Das ist die " + zaehlerLok + "te Wiederholung.");
            zaehlerLok++;
        }while( zaehlerLok <= anzahlWiederholungenPar);
    }
    // Methode Ende
    // ----------------------
}