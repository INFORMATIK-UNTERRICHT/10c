class AufgabenblattBedingteAnweisung{
    // Attribute

    // Attribute Ende
    // ----------------------
    // Konstruktoren
    public AufgabenblattBedingteAnweisung(){
        // Man gibt einen Konstruktor an, auch wenn man keinen braucht.
        // GUTER PROGRAMMIERSTIL
    }
    // Konstruktoren Ende
    // ----------------------
    // Methoden
    public double aufgabe1(double rechnungssumme){
        if(rechnungssumme < 300){
            rechnungssumme = rechnungssumme * 0.98;
        }
        else{
            rechnungssumme = rechnungssumme * 0.95;
        }
        return rechnungssumme;
    }

    public int aufgabe2(int a, int b , int c){
        // Für die Zwischenspeocherung wird eine LOKALE VARIABLE d benutzt
        int d = 0;
        if(a > b){
            d = a;
        }
        else{
            d = b;
        }
        // Jetzt kommt nur eine EINSEITIGE AUSWAHL
        if( c > d){
            d = c;
        }
        // Hier wird die größte der drei Zahlen gesucht,
        // die der Benutzer mit den Parametern a,b und c angibt
        return d;
    }

    public void aufgabe3(double a, double b , double c){
        // Ist a = 0 handfelt es sich NICHT um eine quadratische Gleichung
        if(a == 0){// Beachte "==" bedeutet die Überprüfung der Gleichheit!
            System.err.println("Das ist keine quadratische Gleichung!");
            double d = -c/b;
            System.out.println("Die einzige Lösung lautet: " + d);
        }
        else{
            double diskriminante = b * b - (4 * a * c);
            double loesung1 = 0;
            double loesung2 = 0;
            if (diskriminante < 0){
                System.out.println("Es gibt keine Lösung!");
            }
            else{
                if(diskriminante < 0){
                    loesung1 = -b / (2 * a);
                    System.out.println("Die einzige Lösung lautet: " + loesung1);
                }
                else{
                    // Es gibt also zwei Loesungen :)
                    loesung1 = (-b + Math.sqrt(diskriminante))/2 * a;
                    loesung2 = (-b - Math.sqrt(diskriminante))/2 * a;
                    System.out.println("Die erste Lösung lautet: " + loesung1);
                    System.out.println("Die zweite Lösung lautet: " + loesung2);
                }
            }
        }
    }

    public String aufgabe4(double masse, double koerpergroesse){
        // Bei einem return wird eine Methode sofort verlassen und der
        // Rest NICHT bearbeitet, daher muss man hier nicht
        // jedesmal mit einer zweiseitigen Auswahl arbeiten
        double bmi = masse / (koerpergroesse * koerpergroesse);
        if(bmi <= 19){
            return "Untergewicht";
        }
        if(bmi <= 24){
            return "Normalgewicht";
        }
        if(bmi <= 30){
            return "Übergewicht";
        }
        if(bmi <=40){
            return "Adipositas";
        }
        else{
            return "schwere Adipositas";
        }
    }
    
    public String aufgabe5(int jahr){
        // Hinweis: Möchte man den REST einer DIvision benutzt man das % Zeichen
        int rest = jahr % 400;
        // Steht nur eine Anweisung im Ja bzw. Neinblock der mehrseitigen Auswahl,
        // so kann die geschweifte Klammer weggelassen werden
        if(rest == 0)return "Schaltjahr";
        rest = jahr % 100;
        if(rest == 0) return "Kein Schaltjahr";
        rest = jahr % 4;
        if(rest == 0) return "Schaltjahr";
        else return "Kein Schaltjahr";
    }
    // Methoden Ende
    // ----------------------
}