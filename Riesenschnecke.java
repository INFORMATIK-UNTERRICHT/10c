public class Riesenschnecke
{
    // Attribute
    private int lebensenergie;
    private int anzahlGelegterEier;
    private String aktuellesGeschlecht;
    private String farbeDesSchleims;
    // Attribute Ende 
    // --------------------------------------------
    // Methoden
    public void legeEinEi(){
        anzahlGelegterEier = 1;
    }

    public void legeHundertEier(){
        anzahlGelegterEier = anzahlGelegterEier + 100;
    }

    public void seiWeiblich(){
        aktuellesGeschlecht = "weiblich";
    }

    public void seiMännlich(){
        aktuellesGeschlecht = "männlich";
    }

    public void informationenAusgeben(){
        System.out.println("----------------------------------------------");
        System.out.println("Dies ist mein Zustand: ");
        System.out.println("Attribut lebensenergie: " + lebensenergie);
        System.out.println("Attribut aktuellesGeschlecht: " + aktuellesGeschlecht);
        System.out.println("Attribut anzahlGelegterEier: " + anzahlGelegterEier);
        System.out.println("Meine Klasse ist: " + this.getClass().toString());
    }
    // Methoden Ende
}
