class Zweirad{
    // Attribute
    protected String farbe;
    // Attribute Ende
    // ----------------------
    // Konstruktoren
    // Hier sieht man einen Standardkonstruktor
    public Zweirad(){
        farbe = "rot";
    }
    
    public Zweirad(String farbePar){
        this.farbe = farbePar;
    }
    // Konstruktoren Ende
    // ----------------------
    // Methoden
    public void  informationenAusgeben(){
        System.out.println("Meine Farbe ist: " + this.farbe);
        System.out.println("Ich bin von: " + this.getClass().toString());
    }
    // Methoden Ende
}