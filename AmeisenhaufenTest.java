

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Die Test-Klasse AmeisenhaufenTest.
 *
 * @author  (Ihr Name)
 * @version (eine Versionsnummer oder ein Datum)
 */
public class AmeisenhaufenTest
{
    private Ameisenhaufen ameisenh1;
    private Ameise arbeiti1;
    private Ameise arbeiti2;
    private Ameise soldati1;
    private Ameise koenigi;

    /**
     * Konstruktor fuer die Test-Klasse AmeisenhaufenTest
     */
    public AmeisenhaufenTest()
    {
    }

    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @Before
    public void setUp()
    {
        ameisenh1 = new Ameisenhaufen(10);
        arbeiti1 = new Ameise(1, "Arbeiter");
        arbeiti2 = new Ameise(1, "Arbeiter");
        soldati1 = new Ameise(10, "Soldat");
        koenigi = new Ameise(100, "Königin");
    }

    /**
     * Gibt das Testgerüst wieder frei.
     *
     * Wird nach jeder Testfall-Methode aufgerufen.
     */
    @After
    public void tearDown()
    {
    }
}
