public class Ameisenhaufen{
    // Attribut(e)
    private int maxAnzahlBewohner;
    // Hier wird nicht ein Referenzattribut angelegt,
    // sondern ein Array vom Datentyp Ameise.
    // D.h. handelt es sich gewissermaßen um eine Art
    // Parkplatz für die Objekte.
    private Ameise[] ameisenvolk;
    private int anzahlBelegterPlaetze;
    // Attribut(e) Ende
    // -----------------------------
    // Konstruktor(en)
    public Ameisenhaufen(int maxAnzahl){
        this.maxAnzahlBewohner = maxAnzahl;
        // Bei der Initialisierung eines Arrays geht man
        // dann so vor, wie man das von Objekten kennt,
        // allerdings steht in den eckigen Klammern dann
        // die Anzahl der zu belegenden Plätze (und nicht
        // eine Parameterliste).
        this.ameisenvolk = new Ameise[maxAnzahl];
        this.anzahlBelegterPlaetze = 0;
    }
    // Konstruktor(en) Ende
    // -----------------------------
    // Methoden
    // Nun geht es darum, Ameisenobjekte in das Array
    // einzufügen.
    public boolean ameiseHinzufuegen(
    Ameise neueAmeisePar,
    int platzNummerPar){
        this.ameisenvolk[platzNummerPar] = neueAmeisePar;   
        // Jedesmal wenn eine Ameise in das Array eingefügt wird, erhöht
        // sich der Wert der Integervariable anzahlBelegterPlaetze um Eins. 
        this.anzahlBelegterPlaetze++;
        return true;
    }   

    public void ameiseHinzufuegenDerReiheNach(Ameise neueAmeisePar){
        // Hier beginnt eine sogenannte Zweiseitige Auswahl
        // Der ertse Teil (if Teil bzw. Ja Teil) wird nur ausgeführt, wennn die Bedingung
        // erfüllt ist.
        if(this.anzahlBelegterPlaetze < this.maxAnzahlBewohner){
            // Das Attribut anzahlBelegterPlaetze zählt nicht nur die Anzahl der eingefügten
            // Ameisen mit, sondern hat automatisch auch immer die Nummer des nächsten freien Faches,
            // da die Adressierung des Arrays mit 0 beginnt.
            this.ameisenvolk[this.anzahlBelegterPlaetze] = neueAmeisePar;
            // Nachdem eine neue Ameise eingefügt wurde, wird das Attribut anzahlBelegterPlaetze 
            // um Eins erhöht.
            this.anzahlBelegterPlaetze++;
        }
        // Hier beginnt der Else oder auch Nein-Teil, dieser wird nur ausgeführt, wenn die 
        // obige Bedingung nicht erfüllt, bzw. falsch ist.  
        // Wird der if Teil ausgeführt, wird der else Teil übersprungen UND UMGEKEHRT!
        else{
            System.out.println("Wir haben keinen Platz für Dich! Weg mit Dir!");
        }
    }
    // Methoden Ende
    // -----------------------------
}


