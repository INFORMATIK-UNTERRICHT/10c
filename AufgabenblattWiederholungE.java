public class AufgabenblattWiederholungE{
    // Attribute

    // Attribute Ende
    // ----------------------
    // Konstruktoren
    public AufgabenblattWiederholungE(){
        // Man gibt einen Konstruktor an, auch wenn man keinen braucht.
        // GUTER PROGRAMMIERSTIL
    }
    // Konstruktoren Ende
    // ----------------------
    // Methoden
    // Aufgabe berechneLaufzeit

    public int aufgabe1(int n){
        int rest;
        int teiler = 1;
        do{
            teiler = teiler + 1;
            // Mit % wird eine Division mit Restbildung durchgeführt
            rest = n % teiler;        
        }while(rest != 0);
        return rest;
    }

    public double aufgabe2(double a, double startwertPar, double genauigkeitPar){
        double x = startwertPar ;
        double y = a / x;
        double mittelwert = (x + y) / 2;
        do{
            x = mittelwert;
            y = a / x;
            mittelwert = (x + y) / 2;
            System.out.println("X: " + x + " und Y: " + y + " und Mittelwert: " + mittelwert);
        }while(Math.abs(x-y) > genauigkeitPar);
        return x;
    }

    // Methoden Ende
    // ----------------------
}
