class Tier{
    // Attribute
    protected int lebensenergie;
    // Statt public arbeiten wir hier mit protected
    protected int anzahlBeine;
    // Attribute Ende
    // Konstruktoren
    public Tier(){
        // Hier passier nix
        this.lebensenergie = 11;
        this.anzahlBeine = 4;
    }
    
    public Tier(int lebensenergiePar){
        this.lebensenergie = lebensenergiePar;
        this.anzahlBeine = 4;
    }
    // Konstruktoren Enden
    // Methoden
    public void fresse(){
        this.lebensenergie += 5;
    }
    // Methoden Ende
}